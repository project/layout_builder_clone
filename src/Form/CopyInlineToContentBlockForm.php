<?php

namespace Drupal\layout_builder_clone\Form;

use Drupal\block_content\BlockContentUuidLookup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\entity_clone\Event\EntityCloneEvents;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a form copying an inline block to the block library.
 *
 * @internal
 *   Form classes are internal.
 */
class CopyInlineToContentBlockForm extends FormBase {

  use AjaxFormHelperTrait;
  use LayoutBuilderHighlightTrait;
  use LayoutRebuildTrait;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * The section delta.
   *
   * @var int
   */
  protected $delta;

  /**
   * The region name.
   *
   * @var string
   */
  protected $region;

  /**
   * The component uuid.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The Layout Tempstore.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstore;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The block content UUID lookup service.
   *
   * @var \Drupal\block_content\BlockContentUuidLookup
   */
  protected $uuidLookup;

  /**
   * Constructs a new MoveBlockForm.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\block_content\BlockContentUuidLookup $uuid_lookup
   *   The block content UUID lookup service.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, BlockContentUuidLookup $uuid_lookup) {
    $this->layoutTempstore = $layout_tempstore_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->uuidLookup = $uuid_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('block_content.uuid_lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_clone_copy_inline_to_content_block';
  }

  /**
   * Builds the move block form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage being configured.
   * @param int $delta
   *   The original delta of the section.
   * @param string $region
   *   The original region of the block.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return array
   *   The form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $uuid = NULL) {

    $parameters = array_slice(func_get_args(), 2);
    foreach ($parameters as $parameter) {
      if (is_null($parameter)) {
        throw new \InvalidArgumentException('CopyInlineToContentBlockForm requires all parameters.');
      }
    }

    $this->sectionStorage = $section_storage;
    $this->delta = (int) $delta;
    $this->uuid = $uuid;
    $this->region = $region;

    $section = $this->sectionStorage->getSection($this->delta);
    $original_component = $section->getComponent($this->uuid);
    $block = $original_component->getPlugin();
    $configuration = $block->getConfiguration();

    $form['replace'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace copied block with newly created content library block'),
      '#title_display' => 'after',
      '#default_value' => TRUE,
    ];

    $form['override_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override Admin Label'),
      '#title_display' => 'after',
    ];

    $form['info'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Admin Label'),
      '#default_value' => $configuration['label'],
      '#states' => [
        'visible' => [
          ':input[name="override_info"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Copy to block library'),
      '#button_type' => 'primary',
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
      $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $section = $this->sectionStorage->getSection($this->delta);
    $original_component = $section->getComponent($this->uuid);
    $block = $original_component->getPlugin();
    $configuration = $block->getConfiguration();
    if (!empty($configuration['block_serialized'])) {
      $entity = unserialize($configuration['block_serialized']);
    }
    else {
      $entity = $this->entityTypeManager->getStorage('block_content')->loadRevision($configuration['block_revision_id']);
    }
    if ($form_state->getValue('override_info')) {
      $entity->set('info', $form_state->getValue('info'));
      $label = $entity->label();
      $violations = $entity->validate();
      $violations = $violations->getByField('info');

      foreach ($violations as $violation) {
        /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
        $form_state->setErrorByName(str_replace('.', '][', $violation->getPropertyPath()), $violation->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $section = $this->sectionStorage->getSection($this->delta);
    $original_component = $section->getComponent($this->uuid);
    $block = $original_component->getPlugin();
    $configuration = $block->getConfiguration();
    $block_plugin_id = $block->getPluginDefinition()['id'];

    if ($block_plugin_id !== 'inline_block') {
      return;
    }

    if (!empty($configuration['block_serialized'])) {
      $entity = unserialize($configuration['block_serialized']);
    }
    else {
      $entity = $this->entityTypeManager->getStorage('block_content')->loadRevision($configuration['block_revision_id']);
    }

    $entity_type_definition = $this->entityTypeManager->getDefinition('block_content');
    /** @var \Drupal\entity_clone\EntityClone\EntityCloneInterface $entity_clone_handler */
    $entity_clone_handler = $this->entityTypeManager->getHandler($entity_type_definition->id(), 'entity_clone');
    if ($this->entityTypeManager->hasHandler($entity_type_definition->id(), 'entity_clone_form')) {
      $entity_clone_form_handler = $this->entityTypeManager->getHandler($entity_type_definition->id(), 'entity_clone_form');
    }

    $properties = [];
    if (isset($entity_clone_form_handler) && $entity_clone_form_handler) {
      $properties = $entity_clone_form_handler->getValues($form_state);
    }

    $duplicate = $entity->createDuplicate();
    $duplicate->setReusable();

    $this->eventDispatcher->dispatch(EntityCloneEvents::PRE_CLONE, new EntityCloneEvent($entity, $duplicate, $properties));
    $cloned_entity = $entity_clone_handler->cloneEntity($entity, $duplicate, $properties);
    $this->eventDispatcher->dispatch(EntityCloneEvents::POST_CLONE, new EntityCloneEvent($entity, $duplicate, $properties));

    if ($form_state->getValue('override_info')) {
      $cloned_entity->set('info', $form_state->getValue('info'));
      $cloned_entity->save();
    }
    if ($form_state->getValue('replace')) {
      $configuration['id'] = $cloned_entity->getEntityTypeId() . ':' . $cloned_entity->uuid();
      $configuration['uuid'] = $cloned_entity->uuid();
      $configuration['provider'] = 'block_content';
      // $configuration['provider'] = 'block_content';
      $component = (new SectionComponent(\Drupal::service('uuid')->generate(), $this->region, $configuration));
      $section->insertAfterComponent($original_component->getUuid(), $component);
      $section->removeComponent($this->uuid);
      $this->layoutTempstore->set($this->sectionStorage);
    }
  }

  /**
   * Ajax callback for the region select element.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The components wrapper render array.
   */
  public function getComponentsWrapper(array $form, FormStateInterface $form_state) {
    return $form['components_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    return $this->rebuildAndClose($this->sectionStorage);
  }

  /**
   * Provides a title callback.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The original delta of the section.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return string
   *   The title for the move block form.
   */
  public function title(SectionStorageInterface $section_storage, $delta, $uuid) {
    $block_label = $section_storage
      ->getSection($delta)
      ->getComponent($uuid)
      ->getPlugin()
      ->label();

    return $this->t('Copy the @block_label block', ['@block_label' => $block_label]);
  }

  /**
   * Access check to confirm block type is block_content.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage being configured.
   * @param int $delta
   *   The original delta of the section.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result object.
   */
  public function access(AccountInterface $account, SectionStorageInterface $section_storage, $delta, $uuid) {
    $block = $section_storage
      ->getSection($delta)
      ->getComponent($uuid)
      ->getPlugin();
    return AccessResult::allowedIf($block->getPluginDefinition()['id'] == 'inline_block');
  }

}
